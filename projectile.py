import numpy as np
import pyglet, math
from pyglet.window import key

from resources import projectile_image, sword_ani
from physicalobject import *
from util import *

from time import time

class Projectile(PhysicalObject):
	def __init__(self, img=None, speed=1000, life=1, *args, **kwargs):
		assert img
		super(Projectile, self).__init__(img=img, *args, **kwargs)
		self.key_handler = key.KeyStateHandler()
		self.event_handlers = [self, self.key_handler]

		self.speed = speed

		self.key = 0
		self.lifetime = img.get_duration() * life
		self.time = time()



	def collides_with(self, other_object):
		collision_distance = self.image.width/2 + other_object.image.width/2
		actual_distance = distance(self.position, other_object.position)

		return (actual_distance <= collision_distance)

	def collides_with_any(self, objs):
		col = False
		for obj in objs:
			if self.collides_with(obj):
				col = True
		return col

	def look_at(self, x, y):
		dy = y - self.y
		dx = x - self.x
		self.rotation = math.degrees(-math.atan2(dy, dx))

	def process(self, dt):
		angle_radians = -math.radians(self.rotation)

		self.velocity_x = math.cos(angle_radians) * self.speed
		self.velocity_y = math.sin(angle_radians) * self.speed

	def update(self, dt):
		super(Projectile, self).update(dt)
		self.process(dt)


	def delete(self):
		super(Projectile, self).delete()
