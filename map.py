from wall import *
from util import *
from perlin import *


def load_map(map="resources/map2.map", main_batch=None):
	walls = []
	x = 0
	y = 0
	x1 = 0
	y1 = 0
	x2 = 0
	y2 = 0
	f = open(map, "r")
	for s in f.read():
		if s == '\n':
			x = -50
			y += 50
		if s == "1":
			walls.append(Wall(x - 25, y - 25, x + 25, y + 25, batch=main_batch))
		x += 50
	f.close()
	return walls

def create_cus_map():
	walls = []
	walls.append(Wall(50, 450, 50, 200))
	walls.append(Wall(50, 200, 150, 50))
	walls.append(Wall(150, 50, 600, 50))
	walls.append(Wall(600, 50, 750, 200))
	walls.append(Wall(750, 200, 750, 450))
	walls.append(Wall(600, 600, 750, 450))
	walls.append(Wall(50, 450, 200, 600))
	walls.append(Wall(200, 600, 600, 600))
	walls.append(Wall(200, 350, 200, 300))
	walls.append(Wall(200, 350, 300, 450))
	walls.append(Wall(200, 300, 300, 150))
	walls.append(Wall(500, 450, 300, 450))
	walls.append(Wall(500, 150, 300, 150))
	walls.append(Wall(500, 450, 650, 350))
	walls.append(Wall(500, 150, 650, 300))
	walls.append(Wall(650, 350, 650, 300))
	return walls

def create_perlin_map(points):
	walls = []
	if len(points) == 0:
		return
	start = points[0]
	last = start

	for p in points:
		walls.append(Wall(last[0], last[1], p[0], p[1]))
		last = p
	walls.append(Wall(last[0], last[1], start[0], start[1]))
	return walls

def make_perlin_map():
	walls = []
	walls += create_perlin_map(make_map(200))
	walls += create_perlin_map(make_map(300))
	return walls
	