import math
from noise import pnoise2, snoise2
import numpy as np


def make_map(size = 200, n_points = 16):
	points = []

	a = 0
	while (a < math.pi * 2):
		a += (math.pi * 2) / n_points

		xoff = math.cos(a) 
		yoff = math.sin(a)
		r = (snoise2(xoff, yoff) * 100) + size
		x = ( r * math.cos(a) ) + 400
		y = ( r * math.sin(a) ) + 400

		points.append( (x, y) )

	return points
























