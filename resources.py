import pyglet

def center_image(image):
    """Sets an image's anchor point to its center"""
    image.anchor_x = image.width / 2
    image.anchor_y = image.height / 2

# Tell pyglet where to find the resources
pyglet.resource.path = ['./resources']
pyglet.resource.reindex()

# Load the three main resources and get them to draw centered
projectile_image = pyglet.resource.image("projectile.png")
center_image(projectile_image)

slash_image = pyglet.resource.image("mini.png")
slash_seq = pyglet.image.ImageGrid(slash_image, 1, 3, 90, 80)
slash_ani = pyglet.image.Animation.from_image_sequence(slash_seq[0:], 0.1, False)
for i in slash_ani.frames:
    center_image(i.image)


sword_image = pyglet.resource.image("sword.png")
sword_seq = pyglet.image.ImageGrid(sword_image, 1, 5, 90, 80)
sword_ani = pyglet.image.Animation.from_image_sequence(sword_seq[0:], 0.01, False)
for i in sword_ani.frames:
    center_image(i.image)


player_image = pyglet.resource.image("trooper.png")
center_image(player_image)

wall_image = pyglet.resource.image("wall.png")
center_image(wall_image)


#sword_audio = pyglet.media.load('slash.ogg') 



