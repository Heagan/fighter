import numpy as np
import pyglet, math
from pyglet.window import key

from resources import *
from physicalobject import *
from util import *

from projectile import Projectile

from time import time

LEFT = 1
RIGHT = 2
UP = 3
DOWN = 4
SPACE = 5

class Player(PhysicalObject):

	projectiles = []


	def __init__(self, target=None, *args, **kwargs):
		super(Player, self).__init__(img=player_image, *args, **kwargs)

		self.key_handler = key.KeyStateHandler()
		self.event_handlers = [self, self.key_handler]

		self.speed = 500

		self.key = 0

		self.target = target


	def collides_with(self, other_object):
		collision_distance = self.image.width/2 + other_object.image.width/2
		actual_distance = distance(self.position, other_object.position)

		return (actual_distance <= collision_distance)

	def collides_with_any(self, objs):
		col = False
		for obj in objs:
			if self.collides_with(obj):
				col = True
		return col


	def ranged_attack(self):
		p = Projectile(img=sword_ani, speed=1000, life=20, x=self.x, y=self.y, batch=self.batch)
		p.rotation = self.rotation
		self.projectiles.append(p)

	def melee_attack(self):
		p = Projectile(img=slash_ani, speed=5, life=1, x=self.x, y=self.y, batch=self.batch)
		p.rotation = self.rotation
		self.projectiles.append(p)

	def attack(self, other_object):
		d = distance((self.x, self.y), (other_object.x, other_object.y))
		if d < 150:
			self.melee_attack()
		else:
			self.ranged_attack()
		
	def look_at(self, other_object):
		dy = other_object.y - self.y
		dx = other_object.x - self.x
		self.rotation = math.degrees(-math.atan2(dy, dx))


	def process(self, dt, n):
		self.velocity_x = self.velocity_x / 2 if abs(self.velocity_x / 2) > 1 else 0
		self.velocity_y = self.velocity_y / 2 if abs(self.velocity_y / 2) > 1 else 0

		if n == LEFT:
			self.velocity_x = -self.speed
		if n == RIGHT:
			self.velocity_x = self.speed
		if n == DOWN:
			self.velocity_y = -self.speed
		if n == UP:
			self.velocity_y = self.speed

		if n == SPACE:
			self.attack(self.target)
		
	

	kDown = False
	def update(self, dt):
		super(Player, self).update(dt)

		self.look_at(self.target)

		if self.key_handler[key.UP]:
			self.process(dt, UP)
		if self.key_handler[key.DOWN]:
			self.process(dt, DOWN)
		if self.key_handler[key.LEFT]:
			self.process(dt, LEFT)
		if self.key_handler[key.RIGHT]:
			self.process(dt, RIGHT)
		
		if self.key_handler[key.SPACE]:
			if self.kDown == False:
				self.kDown = True
				self.process(dt, SPACE)
		else:
			self.kDown = False
	
		
		for p in self.projectiles:
			p.update(dt)
			if time() - p.time > p.lifetime:
				p.delete()
				self.projectiles.remove(p)


	def delete(self):
		super(Player, self).delete()
