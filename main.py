from wall import *
from util import *
from perlin import *
from player import *
from map import *

import pyglet, random, math
import numpy as np
import sys

winx = 800
winy = 800

game_window = pyglet.window.Window(winx, winy)
main_batch = pyglet.graphics.Batch()
keyboard = pyglet.window.key.KeyStateHandler()
game_window.push_handlers(keyboard)
# score_label = pyglet.text.Label(text="Score: 0", x=50, y=575, batch=main_batch)

event_stack_size	= 0
walls				= []

POV = (90 * 180) / math.pi
FOV = (70 * 180) / math.pi

player1 = Player(x=winx/2, y=winy/2, batch=main_batch)
player2 = Player(x=winx-150, y=winy-150, batch=main_batch)

player1.target = player2
player2.target = player1

player2.speed = 0

def init():
	global event_stack_size, walls

	# load_map()
	# create_cus_map()
	# walls = make_perlin_map()

	# Clear the event stack of any remaining handlers from other levels
	while event_stack_size > 0:
		game_window.pop_handlers()
		event_stack_size -= 1

	# Add any specified event handlers to the event handler stack
	for handler in player1.event_handlers:
		game_window.push_handlers(handler)
		event_stack_size += 1

	for handler in player2.event_handlers:
		game_window.push_handlers(handler)
		event_stack_size += 1


def draw_walls():
	for w in walls:
		x1 = w.x1
		y1 = w.y1
		x2 = w.x2
		y2 = w.y2
		pyglet.graphics.draw(4, pyglet.gl.GL_LINES, ("v2f", (x1, y1, x2, y2, x1, y1, x2, y2)))


@game_window.event
def on_draw():
	global walls

	game_window.clear()
	pyglet.gl.glLineWidth(2)

	draw_walls()
	
	main_batch.draw()

def update(dt):
	global walls, player1

	player1.collides_with(player2)
	player1.update(dt)
	player2.update(dt)
	player1.process(dt, 0 )

		
def process_args():
	pass

if __name__ == "__main__":

	process_args()
	init()

	# Update the game 120 times per second
	pyglet.clock.schedule_interval(update, 1 / 120.0)
	pyglet.app.run()

































